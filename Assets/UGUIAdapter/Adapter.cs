using UnityEngine;
using UnityEngine.UI;
using UGUIAdapter;
using UGUIAdapter.Data;
using UGUIAdapter.Action;
namespace  UGUIAdapter
{
    public class Adapter:MonoBehaviour   {
	    public string addpterSign = null;       // 适配的数据标示
        public NodeData currentData = null;     //当前适配数据
        [HideInInspector]
        public RectTransform rectTransform = null;
        
        public void Start()
        {   currentData = null;
            #if UNITY_EDITOR
            if (currentData==null){
                AdapterManager.Instance.SetCurentAdatapter(this);
            }
            #else
                AdapterManager.Instance.SetCurentAdatapter(this);
            #endif
            
            if (rectTransform == null){
                rectTransform = gameObject.GetComponent<RectTransform>();
            }
            DoAdaper();
        }
        
        /**
            执行适配
         */
        public void DoAdaper()
        {
            if (currentData==null) return; // 没有适配数据
            NodeAction action = currentData.action;
            if (action !=null)
            {
                action.Addpter(this);
            }
        }
	}
}
