﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UGUIAdapter.Data;
namespace  UGUIAdapter
{
	public class AdapterManager   {
		public static string RELATIVE_RES_PATH = "ToBundle/UIAdapter/";
		public static string ADAPTER_DATA_PATH = "Assets/Resources/" + RELATIVE_RES_PATH;
		

		static AdapterManager _manager;
		public static AdapterManager Instance
		{
			get {
				if(_manager == null)
				{
					_manager = new AdapterManager();
					_manager.Init();
				}
				return _manager;
			}
		}

		public ConfigData config;

		Dictionary<string, NodeData> nodes = null;
		public static string deviceName 
		{
			get
			{
				return 	AdapterManager.Instance.config.deviceName;
			}
		}
		
		private void Init()
		{
			config = Resources.Load<ConfigData>(RELATIVE_RES_PATH + "ConfigData");
			config.action.ParsDevice(config);
			ParsNodes();
		}

		void ParsNodes()
		{
			nodes = new Dictionary<string,NodeData>();
			DeviceData defaultData = config.defaultData;
            if(defaultData==null)return;
                
			DeviceData current = config.current;
			List<NodeData> list = defaultData.list;
			for(int i = 0; i < list.Count;i++)
			{
				NodeData data = list[i];
				nodes.Add(data.sign, data);
			}

			if (current!=null)
			{
				list = current.list;
				for(int i = 0; i<list.Count; i++)
				{
					NodeData node  = list[i];

					if (nodes.ContainsKey(node.sign))
					{
						// 覆盖默认
						nodes[node.sign] =node;
					}
					else
					{
						nodes.Add(node.sign, node);
					}
				}
			}
		}

		public void SetCurentAdatapter(Adapter ui)
		{
			DeviceData current = config.current;
			if (nodes == null || nodes.Count == 0) return;

			if (nodes.ContainsKey(ui.addpterSign))
			{
				ui.currentData = nodes[ui.addpterSign];
			}
		}
	}
}
