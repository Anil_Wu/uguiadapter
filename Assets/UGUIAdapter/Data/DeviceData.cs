/**
    device adapter data
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace UGUIAdapter.Data
{    
    [System.Serializable]
    public  class DeviceData : ScriptableObject
    {
        public string  deviceName  = null;     // 设置需要适配得具体设备名称
        public List<NodeData> list = null;     // 设备的适配数据（包含所有的UI适配数据）
    }
}
