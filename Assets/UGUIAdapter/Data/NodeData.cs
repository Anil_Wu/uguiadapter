using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UGUIAdapter.Action;
using UGUIAdapter.Property;
namespace UGUIAdapter.Data
{
    [CreateAssetMenu(fileName="NodeData", menuName = "UGUIAdapter/NodeData") ]
    public class NodeData : ScriptableObject
    {
        public string  sign          = null;            //适配节点标示	
        public List<NodeProperty>  propertys = null;
        public NodeAction action = null;          //适配执行动作
    }
}