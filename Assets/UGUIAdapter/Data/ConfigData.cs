using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UGUIAdapter.Action;
namespace UGUIAdapter.Data
{
    [CreateAssetMenu(fileName="ConfigData", menuName = "UGUIAdapter/ConfigData") ]
    [System.Serializable]
    public  class ConfigData : ScriptableObject
    {
        public DeviceData current = null;
        public DeviceData defaultData = null;
        public float designWidth = 0 ;        // 设计宽度
        public float desigHeight = 0 ;        // 设计高度
        public ConfigAction action = null;
        private string _deviceName = null;     // 当前设备
        public float widthFactor = 0;
        public float heightFactor = 0;
        public string deviceName
        {
            set
            {
                _deviceName = value;
            }
            
            get
            {
                return _deviceName;
            }
        }
    }
}