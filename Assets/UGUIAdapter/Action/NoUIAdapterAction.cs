using System.Collections.Generic;
using UnityEngine;
using UGUIAdapter;
using UGUIAdapter.Data;
using UGUIAdapter.Property;
namespace UGUIAdapter.Action
{
    // 适配执行
    [CreateAssetMenu(fileName="NoUIAction", menuName = "UGUIAdapter/Action/NoUI") ]
    public  class NoUIAdapterAction : NodeAction
    {
        public override void Addpter(Adapter addpterUI)
        {
            Debug.Log("NoUIAction");
            Transform transform = addpterUI.transform;
            NodeData data = addpterUI.currentData;
            List<NodeProperty>  propertys = data.propertys;
            if (propertys == null) return;
            for(int i = 0; i < propertys.Count;i++)
            {
                NodeProperty item = propertys[i];
                if(item == null ) continue;
                switch(item.type)
                {
                    case NodeProperty.PropertyType.POS:
                        ParsePos(transform,item);
                        break;

                    case NodeProperty.PropertyType.Scale:
                        ParseScale(transform,item);
                        break;
                }
            }
        }

        protected void ParsePos(Transform uiTrans, NodeProperty pos)
        {
            if (pos.isCoverBase)
            {
                uiTrans.localPosition = pos.value;
            }
            else 
            {
                uiTrans.localPosition = pos.value + uiTrans.localPosition;
            }
           
        }

        protected void ParseScale(Transform uiTrans, NodeProperty scale)
        {
            if (scale.isCoverBase)
            {
                uiTrans.localScale = scale.value;
            }
            else 
            {
                uiTrans.localScale = scale.value + uiTrans.localScale;
            }

        }
    }
}