using System.Collections.Generic;
using UnityEngine;

using UGUIAdapter;
using UGUIAdapter.Data;
using UGUIAdapter.Property;
namespace UGUIAdapter.Action
{
    // 适配执行
    [CreateAssetMenu(fileName="NodeAction", menuName = "UGUIAdapter/Action/Node") ]
    public  class NodeAction : ScriptableObject
    {
        public virtual void Addpter(Adapter addpterUI)
        {
            RectTransform rect = addpterUI.rectTransform;
            NodeData data = addpterUI.currentData;
            List<NodeProperty>  propertys = data.propertys;
            if (propertys == null) return;
            for(int i = 0; i < propertys.Count;i++)
            {
                NodeProperty item = propertys[i];
                if(item == null ) continue;

               
                switch(item.type)
                {
                    case NodeProperty.PropertyType.POS:
                        ParsePos(rect,item);
                        break;

                    case NodeProperty.PropertyType.Scale:
                        ParseScale(rect,item);
                        break;

                    case NodeProperty.PropertyType.Size:
                        ParseSize(rect,item);
                        break;
                }
            }
        }

        protected void ParsePos(RectTransform uiTrans, NodeProperty pos)
        {
            if (pos.isCoverBase)
            {
                uiTrans.anchoredPosition3D = pos.value;
            }
            else 
            {
                uiTrans.anchoredPosition3D = pos.value + uiTrans.anchoredPosition3D;
            }
           
        }

        protected void ParseScale(RectTransform uiTrans, NodeProperty scale)
        {
            if (scale.isCoverBase)
            {
                uiTrans.localScale = scale.value;
            }
            else 
            {
                uiTrans.localScale = scale.value + uiTrans.localScale;
            }

        }

        protected void ParseSize(RectTransform uiTrans, NodeProperty size)

        {
            Vector2 v;
            v.x = size.value.x;
            v.y = size.value.y;
            if (size.isCoverBase)
            {
                uiTrans.sizeDelta = v ;
            }
            else 
            {
                uiTrans.sizeDelta = v + uiTrans.sizeDelta;
            }
           
        }
    }
}