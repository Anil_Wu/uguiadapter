using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UGUIAdapter.Action;
using UGUIAdapter.Property;
namespace UGUIAdapter.Action
{
    // 全屏适配
    [CreateAssetMenu(fileName="FullScreenAction", menuName = "UGUIAdapter/Action/FullScreen") ]
    public  class FullScreenAction : NodeAction
    {
        public override void Addpter(Adapter addpterUI)
        {   
            RectTransform uiTrans = addpterUI.rectTransform;
            float wscal = AdapterManager.Instance.config.widthFactor;
            float hscal = AdapterManager.Instance.config.heightFactor;
            Vector2 sizeDelta = uiTrans.sizeDelta;
            if (wscal > hscal)
            {
                float s = (wscal/hscal);
                sizeDelta = sizeDelta * s;
                uiTrans.sizeDelta = sizeDelta;
            }
            else
            {
                float s = (hscal/wscal);
                sizeDelta = sizeDelta * s;
                uiTrans.sizeDelta = sizeDelta;
            }  
            base.Addpter(addpterUI);
        }

       
    }
}