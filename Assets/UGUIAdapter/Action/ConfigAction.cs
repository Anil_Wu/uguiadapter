using UnityEngine;
using UnityEngine.UI;
using UGUIAdapter;
using UGUIAdapter.Data;
using System.Collections;
using System.Collections.Generic;
namespace UGUIAdapter.Action
{
    // 适配执行
    
    [CreateAssetMenu(fileName="ConfigAction", menuName = "UGUIAdapter/Action/Config") ]
    public  class ConfigAction: ScriptableObject
    {
        static string path = AdapterManager.RELATIVE_RES_PATH +  "DevicesData/";
        static string defaultDevice = "Device_1.78";
        public virtual void ParsDevice(ConfigData config)
        {
            float desigHeight   = config.desigHeight;
            float designWidth   = config.designWidth;
            config.widthFactor  = (float)Screen.width/designWidth;
            config.heightFactor = (float)Screen.height/desigHeight;
            config.defaultData = loadDeviceData("Default");
            config.current = null;

            string deviceName = getDeviceName();
            Debug.Log("当前设备分 " +deviceName);
            #if UNITY_EDITOR
                Debug.Log("iphonx");
                if (deviceName == "Device_2.17")
                {
                    config.current = loadDeviceData("IphoneX");
                    return;
                }
            #else
                if (isIphoneX())
                {
                    Debug.Log("iphonx");
                    config.current = loadDeviceData("IphoneX");
                    return;
                }
            #endif

            
            if (deviceName == "Device_1.33") // ipad
            {
                config.current = loadDeviceData("Ipad");
            }
            else 
            {
                
                if (deviceName == defaultDevice)
                {
                    Debug.Log("默认设计分辨率");
                    return;
                }
                else 
                {
                    config.current = loadDeviceData(deviceName);
                }
            }
            
        }

        string getDeviceName()
        {
            string deviceName = "";
            float w = Screen.width;
            float h = Screen.height;
            float p = w/h;
            deviceName =  "Device_" + p.ToString("F2");
            return deviceName;
        }
        DeviceData loadDeviceData(string sign )
        {
            string resPath = path + sign;
            DeviceData current = Resources.Load<DeviceData>(resPath );
            return current;
        }

        bool isIphoneX()
        {
            if (
                UnityEngine.SystemInfo.deviceModel == "iPhone10,3" || 
                UnityEngine.SystemInfo.deviceModel == "iPhone10,6" )
            {
                return true;
            }
            return false;
        }
    }

    
}