using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using UGUIAdapter;
using UGUIAdapter.Data;
using UGUIAdapter.Action;
public class UIAdapterTool:EditorWindow {

	[MenuItem("UIAdapter/refresh AdapterData", false, 0)]
	static void UpAdpterData () {
		ConfigData config = Resources.Load<ConfigData>(AdapterManager.RELATIVE_RES_PATH+"ConfigData");
		if (config == null)
		{
            config = ScriptableObject.CreateInstance<ConfigData>();
            AssetDatabase.CreateAsset(config, AdapterManager.ADAPTER_DATA_PATH + "ConfigData.asset");
		}

		CreateDefault();
		CreateDefaultIphoneX();

		Dictionary<string, Dictionary<string ,NodeData>> devices = new Dictionary<string, Dictionary<string ,NodeData>>();
		getAdapterDeviceName(devices);

		foreach( KeyValuePair<string, Dictionary<string ,NodeData>>kv in devices)
		{
            string savePath = AdapterManager.ADAPTER_DATA_PATH + "DevicesData/"+kv.Key + ".asset";
			DeviceData deviceData =null; 
            deviceData = ScriptableObject.CreateInstance<DeviceData>();
			deviceData.deviceName = kv.Key;
			deviceData.list = new List<NodeData>();
			foreach(KeyValuePair<string, NodeData>kv2 in kv.Value) 
			{
				deviceData.list.Add(kv2.Value);
			}
			
			AssetDatabase.CreateAsset(deviceData,savePath);
            EditorUtility.SetDirty(deviceData);
			if (kv.Key == "Default"){
				config.defaultData = deviceData;
				EditorUtility.SetDirty(config);
			}
				
		}
		
		AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
	}

	public static string getDeviceName(string path)
	{
		string [] list = path.Split('/');
		return list[0];
	}

	public static string getNodSign(string path)
	{
		string [] list = path.Split('/');
		string sign = "";
		for(int i = 1; i<list.Length; i++)
		{
			if (i<(list.Length-1))
			{
				sign =sign + list[i]+"/";
			}
			else
			{
				sign = sign + list[i];
			}
		}
		sign = sign.Replace(".asset", "");
		return sign;
	}

	public static string getPath(NodeData node)
	{
		string path = AssetDatabase.GetAssetPath(node);
        return path;
	}

    public static string getAdpaterDataPath(string path)
    {
        string name = path.Replace(Application.dataPath, "");
        name = name.Replace(AdapterManager.ADAPTER_DATA_PATH, "");
        return name;
    }


	static void getAdapterDeviceName(Dictionary<string, Dictionary<string ,NodeData>> devices)
	{
		NodeData[] list =   Resources.LoadAll<NodeData>(AdapterManager.RELATIVE_RES_PATH);
		for(int i = 0; i < list.Length ; i++)
		{
			NodeData node = list[i];

			string path = getPath(node);
            string adapterDataPath = getAdpaterDataPath(path);
            string deviceName = getDeviceName(adapterDataPath);
            string sign = getNodSign(adapterDataPath);
			Dictionary<string ,NodeData> deviceData = null;
			if (!devices.ContainsKey(deviceName))
			{
				deviceData  = new Dictionary<string ,NodeData>();
				devices.Add(deviceName, deviceData);
			}
			else 
			{
				deviceData = devices[deviceName];
			}
			node.sign = sign;
			deviceData.Add(sign, node);
            EditorUtility.SetDirty(node);
		}
	}

	const  string TopLeft = "TopLeft";
	const  string TopCenter = "TopCenter";
	const  string TopRight = "TopRight";

	const  string Left = "Left";
    const  string Center = "Center";
	const  string Right = "Right";
	
	const  string BottomLeft = "BottomLeft";
    const string BottomCenter = "BottomCenter";
	const  string BottomRight = "BottomRight";

   // [MenuItem("UIAdapter/Default", false, 0)]
    static void CreateDefault()
    {
        CreateNodeData("Default/"+  TopLeft     +".asset");
        CreateNodeData("Default/" + TopCenter   + ".asset");
        CreateNodeData("Default/" + TopRight    + ".asset");

        CreateNodeData("Default/" + Left        + ".asset");
        CreateNodeData("Default/" + Center      + ".asset");
        CreateNodeData("Default/" + Right       + ".asset");

        CreateNodeData("Default/" + BottomLeft  + ".asset");
        CreateNodeData("Default/" + BottomCenter + ".asset");
        CreateNodeData("Default/" + BottomRight + ".asset");

        NodeData node = CreateNodeData("Default/" + "FullScreen" + ".asset");
        node.action = AssetDatabase.LoadAssetAtPath<FullScreenAction>(
            AdapterManager.ADAPTER_DATA_PATH + "Action/FullScreenAction.asset");
        // EditorUtility.SetDirty(node);
        // AssetDatabase.SaveAssets();
    }

    ///[MenuItem("UIAdapter/IphoneX", false, 0)]
    static void CreateDefaultIphoneX()
    {
		int offX = 132;
        NodeData node = CreateNodeData("IphoneX/" + TopLeft + ".asset");
        node.propertys[0].value.x = offX;

        CreateNodeData("IphoneX/" + TopCenter + ".asset");

        node = CreateNodeData("IphoneX/" + TopRight + ".asset");
        node.propertys[0].value.x = -offX;


        node = CreateNodeData("IphoneX/" + Left + ".asset");
        node.propertys[0].value.x = offX;

        CreateNodeData("IphoneX/" + Center + ".asset");

        node = CreateNodeData("IphoneX/" + Right + ".asset");
        node.propertys[0].value.x = -offX;



        node = CreateNodeData("IphoneX/" + BottomLeft + ".asset");
        node.propertys[0].value.x = offX;
        node.propertys[0].value.y = 34;

        node = CreateNodeData("IphoneX/" + BottomCenter + ".asset");
        node.propertys[0].value.y = 34;

        node = CreateNodeData("IphoneX/" + BottomRight + ".asset");
        node.propertys[0].value.x = -offX;
        node.propertys[0].value.y = 34;
        // AssetDatabase.SaveAssets();
        // AssetDatabase.Refresh();
    }

    static NodeData CreateNodeData(string path)
    {
        NodeData node = ScriptableObject.CreateInstance<NodeData>();
        node.propertys = new List<UGUIAdapter.Property.NodeProperty>();
        node.propertys.Add(new UGUIAdapter.Property.NodeProperty());
		node.action = AssetDatabase.LoadAssetAtPath<NodeAction>(AdapterManager.ADAPTER_DATA_PATH + "Action/NodeAction.asset");
        AssetDatabase.CreateAsset(node, AdapterManager.ADAPTER_DATA_PATH + path);
        EditorUtility.SetDirty(node);
        return node;
    }

    [MenuItem("UIAdapter/UIStructure", false, 0)]
	static void AddUIStructure()
	{
		GameObject uiStructure = new GameObject();
		uiStructure.name = "UIStructure";
		uiStructure.AddComponent<RectTransform>();
		Canvas canvas = uiStructure.AddComponent<Canvas>();
		CanvasScaler canvasScaler = uiStructure.AddComponent<CanvasScaler>();
		canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvasScaler.screenMatchMode =  CanvasScaler.ScreenMatchMode.Expand;
		canvasScaler.referenceResolution = new Vector2(1920,1080);
		canvas.renderMode = RenderMode.ScreenSpaceCamera;
		uiStructure.AddComponent<GraphicRaycaster>();

		Transform parent = uiStructure.transform;
		AddReferencePart(parent, TopLeft);
		AddReferencePart(parent, TopCenter);
		AddReferencePart(parent, TopRight);
		AddReferencePart(parent, Left);
		AddReferencePart(parent, Right);
		AddReferencePart(parent, Center);
		AddReferencePart(parent, BottomCenter);
		AddReferencePart(parent, BottomLeft);
		AddReferencePart(parent, BottomRight);
	}

	static void  AddReferencePart(Transform parent, string partName)
	{
		GameObject part = new GameObject();
		part.transform.parent = parent;
		part.name = partName;

        UGUIAdapter.Adapter adapter = part.AddComponent<UGUIAdapter.Adapter>();
        adapter.addpterSign = partName;

		RectTransform rect = part.AddComponent<RectTransform>();
		rect.anchoredPosition3D = Vector3.zero;
		rect.localScale = Vector3.one;
		rect.sizeDelta = Vector2.one;

		switch(partName)
		{
			case TopLeft:
			rect.anchorMax = new Vector2(0,1);
			rect.anchorMin  = new Vector2(0,1);
			break;

			case TopCenter:
			rect.anchorMax = new Vector2(0.5f,1);
			rect.anchorMin = new Vector2(0.5f,1);
			break;

			case TopRight:
				rect.anchorMax = new Vector2(1,1);
				rect.anchorMin = new Vector2(1,1);
			break;

			case Left:
				rect.anchorMax = new Vector2(0,0.5f);
				rect.anchorMin = new Vector2(0,0.5f);
			break;
			
			case Right:
				rect.anchorMax = new Vector2(1, 0.5F);
				rect.anchorMin = new Vector2(1, 0.5F);
			break;

			case Center:
                rect.anchorMax = new Vector2(0.5F, 0.5F);
                rect.anchorMin = new Vector2(0.5F, 0.5F);
			break;

			case BottomCenter:
                rect.anchorMax = new Vector2(0.5F, 0F);
                rect.anchorMin = new Vector2(0.5F, 0F);
			break;

			case BottomLeft:
                rect.anchorMax = Vector2.zero;
                rect.anchorMin = Vector2.zero;
			break;

			case BottomRight:
                rect.anchorMax = new Vector2(1, 0F);
                rect.anchorMin = new Vector2(1, 0F);
			break;

			default:
			break;
		}
	}
}