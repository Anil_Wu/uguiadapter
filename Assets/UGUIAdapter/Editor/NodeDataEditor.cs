using UnityEditor;
using UnityEngine;
using UGUIAdapter.Data;
using UGUIAdapter.Property;
using UGUIAdapter.Action;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(NodeData))]
 public class NodeDataEditor : Editor 
 {
    NodeData data;
    public void OnEnable()
    {
        data = (NodeData)target;

        if(target==null)return;
            
        if (data.propertys==null)
        {
            data.propertys = new List<NodeProperty>();
        }

        if (data.action==null)
        {
            data.action = Resources.Load<NodeAction>("ToBundle/UIAdapter/Action/NodeAction");
        }
       
    }

    NodeProperty CreateProperty( NodeProperty.PropertyType type)
    {
        NodeProperty item = new NodeProperty();
        switch(type)
        {
            case NodeProperty.PropertyType.POS:
                item.value = Vector3.zero;
            break;

            case NodeProperty.PropertyType.Scale:
                item.value = Vector3.one;
            break;

            case NodeProperty.PropertyType.Size:
                item.value = Vector3.zero;
            break;
        }
        item.type = type;
        return item;
    }

    public override void OnInspectorGUI()
    {
        if(data.propertys != null)
        {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField(data.sign);
            int deletIndex = -1;
            for(int i = 0; i < data.propertys.Count;i++)
            {
                NodeProperty item = data.propertys[i];
                if(item == null)
                {
                    item = CreateProperty(NodeProperty.PropertyType.POS);
                }

                item.value = EditorGUILayout.Vector3Field("", item.value);
                item.isCoverBase = EditorGUILayout.Toggle("覆盖",item.isCoverBase);
                
                EditorGUILayout.BeginHorizontal();
                NodeProperty.PropertyType type = (NodeProperty.PropertyType)EditorGUILayout.EnumPopup("", item.type);
                if (type != item.type)
                {
                    data.propertys[i] = CreateProperty(type);
                }

                if (GUILayout.Button("删除"))
                {
                    deletIndex = i;
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            EditorGUILayout.EndVertical();
            if (deletIndex>-1)
            {
                data.propertys.RemoveAt(deletIndex);
            }
        }
        
        data.action = (NodeAction)EditorGUILayout.ObjectField(data.action, typeof(Object), true);
        if(GUILayout.Button("添加适配属性"))
        {
            NodeProperty.PropertyType type = NodeProperty.PropertyType.POS;
            data.propertys.Add(CreateProperty(type));
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(data);
        }
     }
 
 }