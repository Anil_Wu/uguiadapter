using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UGUIAdapter;
using UGUIAdapter.Data;

[CustomEditor(typeof( Adapter))]
 public class AdapterEditor : Editor 
 {
    Adapter data;
    NodeData nodeData = null;
    public void OnEnable()
    {
        data = (Adapter)target;
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical();
        if (data.addpterSign != "" && data.addpterSign != null)
        {
           data.addpterSign = EditorGUILayout.TextField(data.addpterSign);
        }
        else
        {
            nodeData = (NodeData)EditorGUILayout.ObjectField(nodeData,  typeof(NodeData), true);
            string path = UIAdapterTool.getPath(nodeData);
            string adapterDataPath = UIAdapterTool.getAdpaterDataPath(path);
            string sign = UIAdapterTool.getNodSign(adapterDataPath);
            data.addpterSign = sign;
        }

        // data.currentData = (NodeData)EditorGUILayout.ObjectField(data.currentData,  typeof(NodeData), true);
        EditorGUILayout.EndVertical();
        if (GUI.changed)
        {
            EditorUtility.SetDirty(data);
        }
     }
 
 }