
using UnityEditor;
using UnityEngine;
using UGUIAdapter.Data;
using UGUIAdapter.Property;
using UGUIAdapter.Action;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(DeviceData))]
 public class DeviceDataEditor : Editor 
 {
    DeviceData data;
    Vector2 scrollPos;
    public void OnEnable()
    {
        data = (DeviceData)target;
        if (data.list==null)
        {
            data.list = new List<NodeData>();
        }
    }
    
    public override void OnInspectorGUI()
    {
        if(data.list != null)
        {
            scrollPos =  EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(400), GUILayout.Height(600));
            EditorGUILayout.BeginVertical();
            for(int i = 0; i<data.list.Count; i++)
            {
                NodeData item = data.list[i];
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.ObjectField(item, typeof(NodeData), true);
                EditorGUILayout.LabelField(item.sign);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
        }
        
        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
        }
    }
 
 }