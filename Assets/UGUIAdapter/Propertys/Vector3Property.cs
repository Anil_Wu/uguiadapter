using System.Collections;
using UnityEngine;
namespace UGUIAdapter.Property
{
    [System.Serializable]
    public class Vector3Property:NodeProperty
    {
        public Vector3 value;
    }
}
