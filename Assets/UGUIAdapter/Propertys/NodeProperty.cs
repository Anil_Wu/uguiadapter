using System.Collections;
using UnityEngine;
namespace UGUIAdapter.Property
{
    [System.Serializable] 
    public class NodeProperty 
    {
        public enum PropertyType
        {
            POS,
            Scale,
            Size,
        }
        public bool isCoverBase      = false;  			// 是否覆盖基本适配
        public PropertyType type;
        public Vector3 value;
    }
}