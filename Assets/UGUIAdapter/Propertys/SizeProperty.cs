using System.Collections;
using UnityEngine;
namespace UGUIAdapter.Property
{
    [System.Serializable] 
    public class SizeProperty:NodeProperty
    {
        public Vector2 value;
    }
}
