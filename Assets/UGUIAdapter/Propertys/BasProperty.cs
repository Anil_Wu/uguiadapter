
using UnityEngine;

namespace UGUIAdapter.Property
{
    [System.Serializable]
    public class BasProperty : ScriptableObject
    {
        public enum PropertyType
        {
            POS,
            Scale,
            Size,
        }
        public bool isCoverBase      = false;  			// 是否覆盖基本适配
        public PropertyType type;
    }
}